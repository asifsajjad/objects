const keys = require('./keys');

function values(obj) {

    if (!obj || !typeof obj === Object) {
        return [];
    }

    const keyArr = keys(obj);
    let res = [];

    for (let index = 0; index < keyArr.length; index++) {

        if (typeof obj[keyArr[index]] !== 'function') {
            res.push(obj[keyArr[index]]);
        }

    }
    return res;
}

module.exports = values;