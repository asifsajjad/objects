const keys = require('./keys');

function mapObject(obj, cb) {
    if (!obj || !typeof obj === Object || !cb) {
        return {};
    }

    const keyArr = keys(obj);
    let res = {};
    for (let index = 0; index < keyArr.length; index++) {
        let id = keyArr[index];
        res[id] = cb(obj[id], id, obj);
    }
    return res;
}

module.exports = mapObject;