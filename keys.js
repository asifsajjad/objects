function keys(obj) {
    if (!obj || !typeof obj === Object) {
        return [];
    }
    let res = [];
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            res.push(key);
        }
    }
    return res;
}


module.exports = keys;
