const keys= require('./keys');

function defaults(obj,defaultProps)
{
    if (!defaultProps || !typeof defaultProps === Object) {
        if(!obj)
        {
            return {};
        }
        return obj;
    }
    
    let keyArr=keys(defaultProps);
    for(let index=0; index<keyArr.length;index++)
    {
        if(obj[keyArr[index]]===undefined){
            obj[keyArr[index]]=defaultProps[keyArr[index]];
        }
        
    }
    return obj;

}

module.exports=defaults;



// const temp={};
// Object.assign(temp,obj);