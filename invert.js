const keys = require('./keys');
const values = require('./values');

function invert(obj) {
    if (!obj || !typeof obj === Object) {
        return {};
    }

    const keyArr = keys(obj);
    const valArr = values(obj);
    let res = {};

    for (let index = 0; index < keyArr.length; index++) {
        res[valArr[index]] = keyArr[index];
    }

    return res;
}

module.exports=invert;