const keys = require('./keys');

function pairs(obj) {
    if (!obj || !typeof obj === Object) {
        return [];
    }
    const keyArr = keys(obj);
    let res = [];
    for (let index = 0; index < keyArr.length; index++) {
        res[index] = [keyArr[index], obj[keyArr[index]]];
    }

    return res;
}

module.exports = pairs;