const defaults= require('../defaults');

function testDefaults(userObj, userProp)
{
    return defaults(userObj,userProp);
}


const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const testObject2 = { name2: 'Bruce Wayne2', age2: 361, location: 'Gotham2' };

console.log(testDefaults(testObject,testObject2));
console.log(testDefaults(testObject));