const invert = require('../invert');

function testInvert(userObj)
{
    return invert(userObj);
}


const testObject = { name: 'Bruce Wayne', alias: 'Batman', location: 'Gotham' }; 

console.log(invert(testObject));