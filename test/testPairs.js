const pairs = require('../pairs');

function testPairs(userObj) {
    return pairs(userObj);
}

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

console.log(testPairs(testObject));