const keys = require('../keys');

function testKeys(userObj) {
    return keys(userObj);
}

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

console.log(testKeys(testObject));
console.log(testKeys());