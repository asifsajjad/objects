const mapObject=require('../mapObject');

function testMapObject(userObj, userFunc)
{
    return mapObject(userObj,userFunc);
}


const testObject = { one: 1, two: 2, three: 3 };

const abc=(val,id)=>{
    return val*2;
}

console.log(testMapObject(testObject,abc));